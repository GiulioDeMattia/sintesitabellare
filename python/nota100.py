"""
Copyright (c) 2024 Giulio Romano De Mattia

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from nota import Nota


class Nota100(Nota):
    nInstr = 100  # Default instrument number for Nota100

    def __init__(self, dur, freq, ifn1, ifn2, ifn3,amp1,amp2,amp3,ifnA1,ifnA2,ifnA3):
        """
        Initializes a Nota100 object with the specified parameters.

        Args:
        dur (float): Duration of the note.
        freq (float): Frequency of the note.
        ifn1 (int): Function table number 1.
        ifn2 (int): Function table number 2.
        ifn3 (int): Function table number 3.
        """
        super().__init__(dur)
        self.freq = freq  # Frequency of the note
        self.ifn1 = ifn1
        self.ifn2 = ifn2
        self.ifn3 = ifn3
        self.amp1 = amp1
        self.amp2 = amp2
        self.amp3 = amp3
        self.ifnA1 = ifnA1
        self.ifnA2 = ifnA2
        self.ifnA3 = ifnA3

    def toCsound(self):
        """
        Returns a Csound score string representation of the Nota100 object.

        Returns:
        str: Csound score string for the Nota100 object.
        """
        return f"i {Nota100.nInstr}   {self.at}  {self.dur}  {self.freq} {self.ifn1} {self.ifn2} {self.ifn3} {self.amp1} {self.amp2} {self.amp3} {self.ifnA1} {self.ifnA2} {self.ifnA3}\n"
