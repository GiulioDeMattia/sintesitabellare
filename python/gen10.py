from genBase import GenBase
from mathFunc import *
class Gen10(GenBase):
    # Class variable for the function number
    function_number = 100
    def __init__(self, n_GenType, activated_partials, type="prime"):
        """
        Initialize Gen10 instance.

        :param activated_partials: List of activated partials.
        :param another_parameter: Another list parameter for Gen10 (default: None).
        :param n_GenType: Number of GenType parameter (default: 10).
        """
        # Call the constructor of the GenBase class using super()
        super().__init__(n_GenType)
        self.activated_partials = activated_partials
        self.binary_list = self.generate_binary_list(type)
        self.function_number = Gen10.get_and_increment_function_number()

    def generate_binary_list(self,type):
        """
        Generate a binary list based on the activated partials.

        :return: Binary list with 1s for activated partials and 0s for others.
        """
        if not self.activated_partials:
            return ""

        if type == "prime":
            min_value = 1
            max_value = max(self.activated_partials)
            binary_list = "\t".join(str(1 if i in self.activated_partials else 0) for i in range(min_value, max_value + 1))
        elif type == "fib":
            min_value = 1
            max_value = 50
            compareList = [0, 1]
            compareList += list(range(min_value, max_value + 1))
            binary_list = "\t".join(str(1 if i in self.activated_partials else 0) for i in compareList)
            
        return binary_list

    def toCsound(self):
        """
        Convert Gen10 instance to a Csound-formatted string.

        :return: Csound-formatted string.
        """
        csound_string = f"f{self.function_number}\t{self.time}\t{GenBase.size}\t{self.n_GenType}\t{self.binary_list}\n"
        return csound_string

    @classmethod
    def get_and_increment_function_number(cls):
        """
        Get the current function number and increment it by 1 for the next instance.

        :return: Current function number.
        """
        current_function_number = cls.function_number
        cls.function_number += 1
        return current_function_number
