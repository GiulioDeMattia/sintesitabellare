def fibonacci():
    """
    Generator function to generate Fibonacci numbers indefinitely.
    """
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b

def fibonacciList(start_number, length_list):
    """
    Generate a list of Fibonacci numbers starting from a specific number.

    Parameters:
    - start_number (int): The starting number in the Fibonacci sequence.
    - length_list (int): The length of the list to generate.

    Returns:
    - list: A list of Fibonacci numbers.
    """
    fib_sequence = fibonacci()
    for _ in range(start_number):
        next(fib_sequence)
    return [next(fib_sequence) for _ in range(length_list)]

def is_prime(n):
    """
    Check if a number is prime.

    Parameters:
    - n (int): The number to check.

    Returns:
    - bool: True if the number is prime, False otherwise.
    """
    if n <= 1:
        return False
    elif n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True

def primerangeList(start_number, length_list):
    """
    Generate a list of prime numbers starting from a specific number.

    Parameters:
    - start_number (int): The starting number.
    - length_list (int): The length of the list to generate.

    Returns:
    - list: A list of prime numbers.
    """
    primes = []
    num = start_number
    while len(primes) < length_list:
        if is_prime(num):
            primes.append(num)
        num += 1
    return primes
